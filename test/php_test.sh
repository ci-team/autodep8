. $(dirname $0)/helper.sh

test_php() {
  has 'debian/control' 'Source: php-foo\n\nPackage:php-foo\nTestsuite: autopkgtest-pkg-php'
  check_run autodep8
  assertTrue 'runs phpunit-autopkgtest' 'grep "Test-Command: phpunit-autopkgtest" stdout'
  assertTrue 'sets test name' 'grep "Features:.*test-name=" stdout'
}

test_extra_depends() {
  has 'debian/control' 'Source: php-foo\n\nPackage:php-foo\nTestsuite: autopkgtest-pkg-php'
  has 'debian/tests/autopkgtest-pkg-php.conf' 'extra_depends=foobar'
  check_run autodep8
  assertTrue 'uses extra depends' 'grep "Depends:.*, foobar" stdout'
}

test_extra_restrictions() {
  has 'debian/control' 'Source: php-foo\n\nPackage:php-foo\nTestsuite: autopkgtest-pkg-php'
  has 'debian/tests/autopkgtest-pkg-php.conf' 'extra_restrictions=isolation-container'
  check_run autodep8
  assertTrue 'uses extra depends' 'grep "Restrictions:.*, isolation-container" stdout'
}

test_architecture() {
  has 'debian/control' 'Source: php-foo\n\nPackage:php-foo\nTestsuite: autopkgtest-pkg-php'
  has 'debian/tests/autopkgtest-pkg-php.conf' 'architecture=amd64'
  check_run autodep8
  assertTrue 'uses architecture' 'grep "^Architecture: amd64$" stdout'
}

. shunit2
