. $(dirname $0)/helper.sh

test_puppet_module_detection() {
  has debian/control 'Source: puppet-module-puppetlabs-mysql'
  has debian/tests/module_test.pp
  check_run autodep8
}

test_puppet_module_missing_test_manifest() {
  has debian/control 'Source: puppet-module-puppetlabs-mysql'
  run autodep8
  assertEquals 1 "$exitstatus"
}

test_puppet_module_test_command_environment() {
  has debian/control 'Source: puppet-module-puppetlabs-mysql'
  has debian/tests/module_test.pp
  has debian/tests/autopkgtest-pkg-puppet_module.conf 'foo=bar'
  check_run autodep8
  assertTrue "test passes autodep8 config environment to runner script\n$(grep Test-Command: stdout)\n" "grep -q 'Test-Command: pkg_puppet_module_foo=bar' stdout"
}

. shunit2
